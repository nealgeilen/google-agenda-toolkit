from tkinter import *
from tkinter.ttk import *
from tkinter import filedialog
from tkinter import messagebox



class Window(Tk):
    def __init__(self):
        super().__init__()
        self.geometry('800x600')
        self.sTitle = ""

    def setTitle(self, Title):
        self.sTitle += " "+Title

    def Show(self):
        self.title(self.sTitle)
        self.mainloop()
    def Close(self):
        self.destroy()

    def addButton(self, text, command):
        return Button(self, text, command)

    def addLabel(self, text):
        return Label(self,text)