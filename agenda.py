from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
import pickle
import os.path
from datetime import *
SCOPES = ['https://www.googleapis.com/auth/calendar','https://www.googleapis.com/auth/calendar.events']

class Agenda:
    service = None
    aCals = None
    def build_service(self):
        creds = None
        if os.path.exists('token.pickle'):
            with open('token.pickle', 'rb') as token:
                creds = pickle.load(token)
        # If there are no (valid) credentials available, let the user log in.
        if not creds or not creds.valid:
            if creds and creds.expired and creds.refresh_token:
                creds.refresh(Request())
            else:
                flow = InstalledAppFlow.from_client_secrets_file(
                    'credentials.json', SCOPES)
                creds = flow.run_local_server()
            # Save the credentials for the next run
            with open('token.pickle', 'wb') as token:
                pickle.dump(creds, token)

        self.service =  build('calendar', 'v3', credentials=creds)
        return True

    def getCalendars(self):
        if self.aCals is None:
            aCals = []
            for aCal in self.service.calendarList().list().execute()["items"]:
                if aCal["accessRole"] != "reader":
                    aCals.append(aCal)
            self.aCals = aCals
        return  self.aCals

    def getEvents(self, sCalId, timeMin=datetime.utcnow().isoformat() + 'Z', timeMax=None):
        now = datetime.utcnow().isoformat() + 'Z'  # 'Z' indicates UTC time
        return self.service.events().list(calendarId=sCalId, orderBy='startTime', singleEvents=True, timeMin=timeMin, timeMax=timeMax).execute()["items"]

    def deleteEvent(self,  sCalId, sEventId):
        self.service.events().delete(calendarId=sCalId, eventId=sEventId).execute()

    def getEventsWeak(self, sCalId, Weak,Year):
        startWeak = datetime

    def getEvent(self, sCalId, sEventId):
        return self.service.events().get(calendarId=sCalId, eventId=sEventId).execute()

    def get_start_end_dates(self,year, week):
        d = date(year, 1, 1)
        if (d.weekday() <= 3):
            d = d - timedelta(d.weekday())
        else:
            d = d + timedelta(7 - d.weekday())
        dlt = timedelta(days=(week - 1) * 7)
        return d + dlt, d + dlt + timedelta(days=6)

    def insertEvent(self, sCalId, aEventBody):
        return self.service.events().insert(calendarId=sCalId, body=aEventBody).execute()