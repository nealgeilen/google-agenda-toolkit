from windows import *
from agenda import Agenda
from datetime import *
import rfc3339
import json

class selectCal():
    selectedCal = None
    def __init__(self, A):
        if A.build_service():
            W = Window()
            W.setTitle("Selecteer agenda")
            combo = Combobox(W)
            lbl = Label(W, text="Agenda Selectie")
            lbl.grid(column=0, row=1)
            combo.grid(column=1, row=1)
            aNames = []
            for I, Cal in enumerate(A.getCalendars()):
                aNames.insert(I, Cal["summary"])
            combo['values'] = aNames
            combo.current(0)
            def loadCal():
                self.selectedCal = combo.current()
                W.Close()
            b = Button(W, text="Selecteer", command=loadCal)
            b.grid(column=0, row=2)
            W.Show()

class Events():
    selectedEventId = None
    A = Agenda()
    Tree = object
    CalId = None
    def __init__(self, A, Id):
        self.A = A
        self.CalId = Id
        W = Window()
        W.setTitle("Agenda")
        Frame = LabelFrame(W, text="Agenda")
        Frame.pack(fill="both", expand="yes")
        lbl = Label(Frame, text="Test")
        lbl.pack()
        self.buildThree(Frame)
        menu = self.menuConfig(W)

        W.Show()

    def buildThree(self, window):
        Tree = Treeview(window, selectmode="browse", height=40)
        self.Tree = Tree
        Tree['columns'] = ('name', 'startdate', 'enddate')
        Tree.heading("#0", text="Id")
        Tree.heading("name", text="Naam")
        Tree.heading("startdate", text="Start Datum")
        Tree.heading("enddate", text="Eind Datum")
        Tree.bind('<ButtonRelease-1>', self.onSelect)
        Tree.pack(side=TOP, fill=X)
        self.loadThree()

    def loadThree(self):
        for i in self.Tree.get_children():
            self.Tree.delete(i)
        for I, Event in enumerate(self.A.getEvents(self.CalId)):
            if "dateTime" in  Event["start"]:
                self.Tree.insert("", "end", text=Event["id"], values=(Event["summary"], Event["start"]["dateTime"], Event["end"]["dateTime"]))


    def menuConfig(self, W):
        Mainmenu = Menu(W)

        EventMenu = Menu(Mainmenu, tearoff=0)
        EventMenu.add_command(label="Delete", command= lambda: (self.A.deleteEvent(self.CalId, self.selectedEventId), self.loadThree()))
        Mainmenu.add_cascade(label="Event", menu=EventMenu)
        CleanerMenu = Menu(Mainmenu, tearoff=0)
        CleanerMenu.add_command(label="24 uur", command= lambda: self.cleaner(24))
        CleanerMenu.add_command(label="48 uur", command= lambda: self.cleaner(48))
        CleanerMenu.add_command(label="72 uur", command= lambda: self.cleaner(72))
        CleanerMenu.add_command(label="168 uur", command= lambda: self.cleaner(168))
        Mainmenu.add_cascade(label="Cleaner", menu=CleanerMenu)
        Mainmenu.add_command(label="Vullen data dump", command= lambda: (self.getDataFiller()))
        Mainmenu.add_command(label="Afsluiten", command=W.quit)
        W.config(menu=Mainmenu)
        return Mainmenu

    def onSelect(self, evt):
        curItem = self.Tree.focus()
        self.selectedEventId = self.Tree.item(curItem)["text"]

    def getDataFiller(self):
        sFileLocation = filedialog.askopenfilename(initialdir="/", title="Select file",
                                   filetypes=(("Data bestand", "*.json"), ("all files", "*.*")))

        if(messagebox.askyesno("Agenda Toolkit", "Weet je zeker dat je deze data wilt gebruiken: " + sFileLocation)):
            with open(sFileLocation) as json_file:
                data = json.load(json_file)
                period = data['period']
                aDays = data['days']
                startDate = datetime(period['Start']['year'], period['Start']['month'],
                                     period['Start']['day'])  # start date
                endDate = datetime(period['End']['year'], period['End']['month'], period['End']['day'])  # end date

                delta = endDate - startDate

                # loop all days inside period
                for i in range(delta.days + 1):
                    oDay = startDate + timedelta(i)
                    ISODay = oDay.isocalendar()

                    nDay = ISODay[2]
                    nYear = ISODay[0]
                    nWeek = ISODay[1]
                    nMonth = oDay.month

                    if nWeek not in period['skipWeeks']:
                        if nDay <= 5:
                            aDay = aDays[(nDay - 1)]
                            for aEvent in aDay:
                                startEventTime = datetime(year=nYear, month=nMonth, day=oDay.day,
                                                          hour=aEvent['Start']['hour'],
                                                          minute=aEvent['Start']['minute'])
                                endEventTime = datetime(year=nYear, month=nMonth, day=oDay.day,
                                                        hour=aEvent['End']['hour'], minute=aEvent['End']['minute'])

                                GoogleEvent = {
                                    'summary': aEvent['Title'],
                                    'location': aEvent['Location'],
                                    'description': '',
                                    'start': {
                                        'dateTime': rfc3339.rfc3339(startEventTime),
                                    },
                                    'end': {
                                        'dateTime': rfc3339.rfc3339(endEventTime)
                                    },
                                }
                                self.A.insertEvent(self.CalId, GoogleEvent)
                self.loadThree()





    def cleaner(self, iHours):
        if self.selectedEventId != "":
            aEvent = self.A.getEvent(self.CalId, self.selectedEventId)
            startDate = aEvent["start"]["dateTime"][0:19]
            startDateTime = datetime.strptime(startDate, '%Y-%m-%dT%H:%M:%S')
            endDateTime = startDateTime + timedelta(hours=iHours)
            print(startDateTime, endDateTime)

            aCalList = self.A.getEvents(self.CalId, timeMax=rfc3339.rfc3339(endDateTime), timeMin=rfc3339.rfc3339(startDateTime))
            print(aCalList)
            for aEvent in aCalList:
                self.A.deleteEvent(self.CalId, aEvent["id"])

            self.loadThree()


